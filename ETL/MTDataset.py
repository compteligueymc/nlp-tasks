from __future__ import annotations
import pathlib
import random
import tensorflow as tf
from tensorflow.keras.layers import TextVectorization
from typing import Any

class MTDatasetFactory:
    def __init__(self, sourceVectorizer, targetVectorizer) -> None:
        self.sourceVectorizer = sourceVectorizer
        self.targetVectorizer = targetVectorizer
    
    def ApplyVectorization(self, source, target):
        sourceVector = self.sourceVectorizer(source)
        targetVector = self.targetVectorizer(target)
        return (
            {
                "model_encoder_inputs": sourceVector,
                "model_decoder_inputs": targetVector[:, :-1],
            },
            targetVector[:, 1:],
        )    
        
    def __call__(self, textPairs, batchSize: int = 32, shuffle: bool = False) -> Any:
        sourceTexts, targetTexts = zip(*textPairs)
        sourceTexts = list(sourceTexts)
        targetTexts = list(targetTexts)
        dataset = tf.data.Dataset.from_tensor_slices((sourceTexts, targetTexts))
        dataset = dataset.batch(batchSize)
        dataset = dataset.map(self.ApplyVectorization)
        if shuffle:
            dataset = dataset.shuffle(2048)
        return dataset.prefetch(16).cache()

def GetDatasets(textFile: str|pathlib.Path, vocabSize: int = 13000, batchSize: int = 32):
    text_pairs = ParseText(textFile)
    train_pairs, val_pairs = Split(text_pairs)
    sourceVectorizer, targetVectorizer = GetVectorizers(train_pairs, vocabSize=vocabSize)
    trainDataset = MTDatasetFactory(sourceVectorizer, targetVectorizer)(train_pairs, batchSize=batchSize, shuffle=True)
    valDataset = MTDatasetFactory(sourceVectorizer, targetVectorizer)(val_pairs, batchSize=batchSize, shuffle=False)
    return trainDataset, valDataset

def GetVectorizers(train_pairs, vocabSize: int = 13000, sequenceLength: int = 20):
    sourceVectorizer = TextVectorization(
    max_tokens=vocabSize,
    output_mode="int",
    output_sequence_length=sequenceLength,
    standardize='lower',
    split= "whitespace",
    )
    targetVectorizer = TextVectorization(
        max_tokens=vocabSize,
        output_mode="int",
        output_sequence_length=sequenceLength + 1,
        standardize='lower',
    )
    train_eng_texts = [pair[0] for pair in train_pairs]
    train_spa_texts = [pair[1] for pair in train_pairs]
    sourceVectorizer.adapt(train_eng_texts)
    targetVectorizer.adapt(train_spa_texts)
    return sourceVectorizer, targetVectorizer

def Split(text_pairs, maxSize:int = 20000):
    random.shuffle(text_pairs)
    text_pairs = text_pairs[:maxSize]
    num_val_samples = int(0.15 * len(text_pairs))
    num_train_samples = len(text_pairs) - 2 * num_val_samples
    train_pairs = text_pairs[:num_train_samples]
    val_pairs = text_pairs[num_train_samples:]
    return train_pairs, val_pairs

def ParseText(textFile: str| pathlib.Path):
    text_pairs = []
    with open(textFile, encoding='utf-8') as f:
        lines = f.read().split("\n")[:-1]
        for line in lines:
            eng, spa = line.split("\t")
            spa = "[start] " + spa + " [end]"
            text_pairs.append((eng, spa))
    return text_pairs


if __name__ == "__main__":
    """
    Let's take a quick look at the sequence shapes
    (we have batches of 64 pairs, and all sequences are 20 steps long):
    """
    textFilePath = pathlib.Path(__file__).parent / "datasets" / "spa-eng" / "spa.txt"
    trainDataset, _ = GetDatasets(textFilePath)

    for inputs, targets in trainDataset.take(1):
        print(f'inputs["encoder_inputs"].shape: {inputs["model_encoder_inputs"].shape}')
        print(f'inputs["decoder_inputs"].shape: {inputs["model_decoder_inputs"].shape}')
        print(f"targets.shape: {targets.shape}")
