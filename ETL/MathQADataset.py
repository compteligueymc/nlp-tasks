from functools import partial
from typing import Tuple
from datasets import load_dataset
from datasets.dataset_dict import DatasetDict
from datasets.arrow_dataset import Example
from transformers import BertTokenizer
import numpy as np
import tensorflow as tf
from transformers import DataCollatorWithPadding, DataCollatorForSeq2Seq
# """
# Now let's create and train a custom tokenizer"""

# mathDataset = load_dataset('math_dataset', 'algebra__linear_1d')
# text = "Ceci est un texte."
# text = ' '.join(list(text))
# text = f"[CLS] {text} [SEP]"
# # il faut ajouter sep a la reponse
# question = mathDataset['train'][0]["question"]

# tokenizer = BertTokenizer(vocab_file="./mathVocabulary.txt", do_lower_case=True, padding_side="left")
# input_ids = tokenizer.encode(text, add_special_tokens=False, return_tensors="tf")
# input_ids = tokenizer.encode(text, add_special_tokens=False, return_tensors="tf")

# print(input_ids)


class MathQADatasetFactory:
    datasetName = "math_dataset"

    def __init__(self) -> None:
        pass

    def __call__(self, subset: str, tokenizer: BertTokenizer) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
        dataset = load_dataset(self.datasetName, subset)
        # maybe we can split data before
        dataset = dataset["test"].train_test_split(test_size=0.1)
        dataset = dataset.map(self._FormatText)
        dataset = dataset.map(partial(self._ApplyTokenization, tokenizer))
        # dataset = dataset.map(self._UpdateFieldsName, remove_columns=["question", "answer"])
        trainDs, validDs = self._ConvertToTFDataset(dataset, tokenizer)
        return trainDs, validDs

    def _ConvertToTFDataset(self, dataset: DatasetDict, tokenizer: BertTokenizer) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
        dataCollator = DataCollatorForSeq2Seq(tokenizer, return_tensors="tf")
        tfTrainDataset = dataset["train"].to_tf_dataset(
            columns=["input_ids", "token_type_ids", "attention_mask", "labels"],
            shuffle=True,
            batch_size=16,
            collate_fn=dataCollator,
        )

        tfValidationDataset = dataset["test"].to_tf_dataset(
            columns=["input_ids", "token_type_ids", "attention_mask", "labels"],
            shuffle=False,
            batch_size=16,
            collate_fn=dataCollator,
        )
        return tfTrainDataset, tfValidationDataset

    def _FormatText(self, datasetExample: Example) -> Example:
        return Example(
            {fieldName: self._TextToChars(fieldText) for fieldName, fieldText in datasetExample.items()}
        )

    def _ApplyTokenization(self, tokenizer: BertTokenizer, datasetExample: Example) -> Example:
        # return Example(
        #     {fieldName: self._TokenizeText(tokenizer, fieldText) for fieldName, fieldText in datasetExample.items()}
        # )
        modelInputs = tokenizer(datasetExample["question"], add_special_tokens=False, return_tensors="tf")
        with tokenizer.as_target_tokenizer():
            labels = tokenizer(datasetExample["answer"], add_special_tokens=False, return_tensors="tf")
        modelInputs["labels"] = labels["input_ids"]
        # [:, 1:]
        return Example(
            {fieldName: fieldValue[0] for fieldName, fieldValue in modelInputs.items()}
        )

    @staticmethod
    def _TokenizeText(tokenizer: BertTokenizer, text: str) -> tf.Tensor:
        tokenIds = tokenizer.encode(text, add_special_tokens=True, return_tensors="tf")
        return tokenIds[0]

    @staticmethod
    def _TextToChars(text: str) -> str:
        text = text.replace("b'", "")
        text = text.replace("\\n'", "")
        text = ' '.join(list(text))
        return text

    @staticmethod
    def _UpdateFieldsName(datasetExample: Example) -> Example:
        return Example(
            {
                "input_ids": datasetExample["question"],
                "labels": datasetExample["answer"]
            })


if __name__ == "__main__":
    SUBSET = 'algebra__linear_1d'
    text = "Ceci est un texte."
    tokenizer = BertTokenizer(vocab_file="./mathVocabulary.txt", do_lower_case=True, padding_side="left")
    ds, _ = MathQADatasetFactory()(SUBSET, tokenizer)
    for inputs in ds.take(1):
        print(f'inputs["encoder_inputs"].shape: {inputs["input_ids"].shape}')
        print(f'inputs["decoder_inputs"].shape: {inputs["labels"].shape}')
