# Inspired from https://github.com/keras-team/keras-io/tree/master/examples/nlp

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from typing import Any, Dict

class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim: int, dense_dim: int, num_heads: int, **kwargs) -> None:
        """Transformer blocks with multihead self-attention.

        Args:
            embed_dim (int): dimension of query and value vectors.
            dense_dim (int): dimension of linear layer inside the block
            num_heads (int): number of self-attention layer
        """
        super().__init__(**kwargs)
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.attention = layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim
        )
        self.dense_proj = keras.Sequential(
            [
                layers.Dense(dense_dim, activation="relu"),
                layers.Dense(embed_dim),
            ]
        )
        self.layernorm_1 = layers.LayerNormalization()
        self.layernorm_2 = layers.LayerNormalization()
        self.supports_masking = True

    def call(self, inputs: tf.Tensor, mask=None)-> tf.Tensor:
        if mask is not None:
            padding_mask = tf.cast(mask[:, tf.newaxis, tf.newaxis, :], dtype="int32")
        attention_output = self.attention(
            query=inputs, value=inputs, key=inputs, attention_mask=padding_mask
        )
        proj_input = self.layernorm_1(inputs + attention_output)
        proj_output = self.dense_proj(proj_input)
        return self.layernorm_2(proj_input + proj_output)


class TokenEmbedding(layers.Layer):
    """ Embedding layer that consists of a token embedding and positional embedding."""
    def __init__(self, sequence_length: int, vocab_size: int, embed_dim: int, **kwargs) -> None:
        """Initializer.

        Args:
            sequence_length (int): length of sequences to embed.
            vocab_size (int): vocabulary size.
            embed_dim (int): dimension of emmbedding.
        """
        super().__init__(**kwargs)
        self.token_embeddings = layers.Embedding(
            input_dim=vocab_size, output_dim=embed_dim
        )
        self.position_embeddings = layers.Embedding(
            input_dim=sequence_length, output_dim=embed_dim
        )
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.embed_dim = embed_dim

    def call(self, inputs: tf.Tensor) -> tf.Tensor:
        length = tf.shape(inputs)[-1]
        positions = tf.range(start=0, limit=length, delta=1)
        embedded_tokens = self.token_embeddings(inputs)
        embedded_positions = self.position_embeddings(positions)
        return embedded_tokens + embedded_positions

    def compute_mask(self, inputs: tf.Tensor, mask=None) -> tf.Tensor:
        return tf.math.not_equal(inputs, 0)


class TransformerDecoder(layers.Layer):
    """Transformer decoder that consists of self-attention and cross-attention layer."""
    def __init__(self, embed_dim: int, latent_dim: int, num_heads: int, **kwargs) -> None:
        super().__init__(**kwargs)
        self.embed_dim = embed_dim
        self.latent_dim = latent_dim
        self.num_heads = num_heads
        self.attention_1 = layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim
        )
        self.attention_2 = layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim
        )
        self.dense_proj = keras.Sequential(
            [
                layers.Dense(latent_dim, activation="relu"),
                layers.Dense(embed_dim),
            ]
        )
        self.layernorm_1 = layers.LayerNormalization()
        self.layernorm_2 = layers.LayerNormalization()
        self.layernorm_3 = layers.LayerNormalization()
        self.supports_masking = True

    def call(self, inputs: tf.Tensor, encoder_outputs: tf.Tensor, mask=None) -> tf.Tensor:
        causal_mask = self.get_causal_attention_mask(inputs)
        if mask is not None:
            padding_mask = tf.cast(mask[:, tf.newaxis, :], dtype="int32")
            padding_mask = tf.minimum(padding_mask, causal_mask)

        attention_output_1 = self.attention_1(
            query=inputs, value=inputs, key=inputs, attention_mask=causal_mask
        )
        out_1 = self.layernorm_1(inputs + attention_output_1)

        attention_output_2 = self.attention_2(
            query=out_1,
            value=encoder_outputs,
            key=encoder_outputs,
            attention_mask=padding_mask,
        )
        out_2 = self.layernorm_2(out_1 + attention_output_2)

        proj_output = self.dense_proj(out_2)
        return self.layernorm_3(out_2 + proj_output)

    def get_causal_attention_mask(self, inputs: tf.Tensor) -> tf.Tensor:
        input_shape = tf.shape(inputs)
        batch_size, sequence_length = input_shape[0], input_shape[1]
        i = tf.range(sequence_length)[:, tf.newaxis]
        j = tf.range(sequence_length)
        mask = tf.cast(i >= j, dtype="int32")
        mask = tf.reshape(mask, (1, input_shape[1], input_shape[1]))
        mult = tf.concat(
            [tf.expand_dims(batch_size, -1), tf.constant([1, 1], dtype=tf.int32)],
            axis=0,
        )
        return tf.tile(mask, mult)


class EncoderDecoderModelFactory:
    """Create a compiled encoder-decoder model."""
    def __init__(self, compilingParameters: Dict[str, Any], plotModel: bool = True) -> None:
        self.plotModel = plotModel
        self._compilingParameters = compilingParameters

    def __call__(self, sequence_length: int, vocab_size: int, embed_dim: int = 256, latent_dim: int = 2048, num_heads: int = 3 ) -> keras.Model:
        """Create Encoder-decoder model with Transformers."""
        # 1st step - Encode inputs
        encoder_inputs = keras.Input(shape=(None,), dtype="int64", name="model_encoder_inputs")
        decoder_inputs = keras.Input(shape=(None,), dtype="int64", name="model_decoder_inputs")

        encoder = self._BuildEncoderModel(sequence_length, vocab_size, embed_dim, latent_dim, num_heads)
        decoder = self._BuildDecoderModel(sequence_length, vocab_size, embed_dim, latent_dim, num_heads)

        encoder_outputs = encoder(encoder_inputs)
        decoder_outputs = decoder([decoder_inputs, encoder_outputs])

        model = keras.Model(
            [encoder_inputs, decoder_inputs], decoder_outputs, name="model_encoder_decoder"
        )

        if self.plotModel:
            keras.utils.plot_model(encoder, to_file='encoder.png', show_shapes=True, expand_nested=True)
            keras.utils.plot_model(decoder, to_file='decoder.png', show_shapes=True, expand_nested=True)
            keras.utils.plot_model(model, to_file='model.png', show_shapes=True, expand_nested=True)
        
        self._Compile(model)
        
        return model

    def _BuildEncoderModel(self, sequence_length: int, vocab_size: int, embed_dim: int = 256, latent_dim: int = 2048, num_heads: int = 3 ) -> keras.Model:
        encoder_inputs = keras.Input(shape=(None,), dtype="int64", name="encoder_inputs")
        x = TokenEmbedding(sequence_length, vocab_size, embed_dim)(encoder_inputs)
        encoder_outputs = TransformerBlock(embed_dim, latent_dim, num_heads)(x)
        encoder = keras.Model(encoder_inputs, encoder_outputs)
        return encoder

    def _BuildDecoderModel(self, sequence_length: int, vocab_size: int, embed_dim: int = 256, latent_dim: int = 2048, num_heads: int = 3 ) -> keras.Model:
        decoder_inputs = keras.Input(shape=(None,), dtype="int64", name="decoder_inputs")
        encoded_seq_inputs = keras.Input(shape=(None, embed_dim), name="decoder_state_inputs")
        x = TokenEmbedding(sequence_length, vocab_size, embed_dim)(decoder_inputs) 
        y = TransformerDecoder(embed_dim, latent_dim, num_heads)(x, encoded_seq_inputs)
        y = layers.Dropout(0.5)(y)
        decoder_outputs = layers.Dense(vocab_size, activation="softmax")(y)
        decoder = keras.Model([decoder_inputs, encoded_seq_inputs], decoder_outputs)
        return decoder
    
    def _Compile(self, model: keras.Model) -> None:
        optimizer = self._GetOptimizer()
        model.compile(
            optimizer=optimizer,
            loss=self._compilingParameters.get("lossName", "sparse_categorical_crossentropy"),
            metrics=self._compilingParameters.get("metrics", ["accuracy"]),
        )
    
    def _GetOptimizer(self) -> keras.optimizers.Optimizer:
        learningRate = self._compilingParameters.get("learningRate", 1e-4)
        optimizerName = self._compilingParameters.get("otpimizer", "Adam")
        if optimizerName == "Adam":
            optimizer = tf.keras.optimizers.Adam(learning_rate=learningRate)
        if optimizerName == "RMSprop":
            optimizer = keras.optimizers.RMSprop(learning_rate=learningRate)
        return optimizer
        


if __name__ == "__main__":
    sequence_length = 20
    vocab_size = 13000
    model = EncoderDecoderModelFactory({}, False)(sequence_length, vocab_size)
    print(model.summary())