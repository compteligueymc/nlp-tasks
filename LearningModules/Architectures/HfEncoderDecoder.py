import os
from typing import Any, Dict
from transformers import (BertConfig, BertTokenizer, EncoderDecoderConfig, TFEncoderDecoderModel)
from tempfile import TemporaryDirectory


class BertEncoderDecoderFactory:

    def __init__(self) -> None:
        pass

    def __call__(self, bertConfigParameters: Dict[str, Any], tokenizer: BertTokenizer) -> TFEncoderDecoderModel:
        bertConfigParameters.update(
            dict(
                vocab_size=tokenizer.vocab_size,
                bos_token_id=tokenizer.cls_token_id,
                eos_token_id=tokenizer.sep_token_id,
                pad_token_id=tokenizer.pad_token_id,
            )
        )
        with TemporaryDirectory() as tempDir:

            self._BuildAndSaveBertConfig(bertConfigParameters, saveDirectory=tempDir)
            self._BuildAndSaveModelConfig(tempDir)

            # Loading model and config from pretrained folder
            encoder_decoder_config = EncoderDecoderConfig.from_pretrained(tempDir)
            model = TFEncoderDecoderModel.from_pretrained(tempDir, config=encoder_decoder_config)
            return model

    def _BuildAndSaveModelConfig(self, configDirectory: str) -> None:
        configFilePath = os.path.join(configDirectory, "config.json")
        # Initializing a BERT bert-base-uncased style configuration
        config_encoder = BertConfig.from_pretrained(configFilePath)
        config_decoder = BertConfig.from_pretrained(configFilePath)

        config = EncoderDecoderConfig.from_encoder_decoder_configs(config_encoder, config_decoder)

        # Initializing a Bert2Bert model from the bert-base-uncased style configurations
        model = TFEncoderDecoderModel(config=config)

        # Accessing the model configuration
        config_encoder = model.config.encoder
        config_decoder = model.config.decoder
        # set decoder config to causal lm
        config_decoder.is_decoder = True
        config_decoder.add_cross_attention = True
        # set pad token id & start token
        config.pad_token_id = config_decoder.pad_token_id
        config.decoder_start_token_id = config_decoder.bos_token_id

        # Saving the model, including its configuration
        model.save_pretrained(configDirectory)

    @staticmethod
    def _BuildAndSaveBertConfig(bertConfigParameters: Dict[str, Any], saveDirectory: str) -> None:
        config = BertConfig.from_pretrained("bert-base-uncased", **bertConfigParameters)
        config.save_pretrained(save_directory=saveDirectory)


if __name__ == "__main__":
    tokenizer = BertTokenizer(vocab_file="./mathVocabulary.txt", do_lower_case=True, padding_side="left")
    bertConfigParameters = dict(
        hidden_dropout_prob=0.2,
        hidden_act="relu",
        hidden_size=128,
        num_attention_heads=2,
        num_hidden_layers=2,
        intermediate_size=128,
    )
    model = BertEncoderDecoderFactory()(bertConfigParameters, tokenizer)
    print(model)