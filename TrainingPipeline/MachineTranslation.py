from __future__ import annotations
import pathlib

from typing import Any, Dict
import tensorflow as tf
from LearningModules.Architectures import EncoderDecoderModelFactory
from ETL.MTDataset import GetDatasets

def LaunchTraining(textFilePath: str|pathlib.Path, parameters: Dict[str, Any]) -> None:
    
    trainDataset, validDataset = GetDatasets(textFilePath, parameters["vocabSize"], parameters["batchSize"])
    transformer = EncoderDecoderModelFactory({}, False)(parameters["sequence_length"], parameters["vocabSize"], parameters["embed_dim"], parameters["latent_dim"], parameters["num_heads"])
    transformer.fit(trainDataset, epochs=parameters["epochs"], validation_data=validDataset, verbose=2)

if __name__ == "__main__":
    TEXT_FILEPATH = pathlib.Path(__file__).parents[2].joinpath("ETL/datasets/spa-eng/spa.txt")
    PARAMETERS = dict(
        vocabSize = 13000, 
        batchSize = 32, 
        sequence_length = 20, 
        embed_dim = 128, 
        latent_dim = 256, 
        num_heads = 2,
        epochs=10,
        )
    LaunchTraining(TEXT_FILEPATH, PARAMETERS)
