from typing import Dict, Any

import tensorflow as tf
from tensorflow import keras
from transformers import BertTokenizer

from LearningModules.Architectures import BertEncoderDecoderFactory
from ETL.MathQADataset import MathQADatasetFactory


class CategoricalAccuracy(keras.metrics.Accuracy):
    def __init__(self, name='accuracy_', dtype=None) -> None:
        super().__init__(name, dtype)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = tf.argmax(y_pred, axis=-1)
        super().update_state(y_true, y_pred)


def LaunchTraining(vocabFile: str, subset: str, parameters: Dict[str, Any]) -> Any:
    tokenizer = BertTokenizer(vocab_file=vocabFile, do_lower_case=True, padding_side="left")
    trainDataset, validDataset = MathQADatasetFactory()(subset, tokenizer)
    model = BertEncoderDecoderFactory()(parameters["modelParameters"], tokenizer)
    # Test call
    # inp = trainDataset.take(1)
    # out = model(*inp)
    ###
    optimizer = keras.optimizers.Adam(learning_rate=parameters["lr"])
    model.compile(optimizer, metrics=[CategoricalAccuracy()], run_eagerly=None)
    hist = model.fit(trainDataset, epochs=parameters["epochs"], validation_data=validDataset, verbose=2)
    return hist, model, tokenizer


if __name__ == "__main__":
    vocabFile = "./mathVocabulary.txt"
    subset = "algebra__linear_1d"
    parameters = {
        "modelParameters": dict(
            hidden_dropout_prob=0.0,
            hidden_act="gelu",
            hidden_size=512,
            num_attention_heads=8,
            num_hidden_layers=8,
            intermediate_size=512,
            initializer_range=0.5,
            max_position_embeddings=50,
        ),
        "lr": 1e-2,
        "weightDecay": 1e-5,
        "epochs": 50
    }
    trainingOutputs = LaunchTraining(vocabFile, subset, parameters)
