# NLP tasks projects

We host projects about nlp tasks

### Machine translation with Keras

In [MachineTranslation.py](https://gitlab.com/compteligueymc/nlp-tasks/-/blob/main/TrainingPipeline/MachineTranslation.py) we developed a pipeline for machine translation using a encoder-decoder model with Transformers.

![This is an image](model.png)

### Solve mathematical problems using Transformers.
We try to reproduce experiments presented in **ANALYSING MATHEMATICAL REASONING ABILITIES
OF NEURAL MODELS.** by Saxton D. et al. We use "math-dataset" from TensorFlow datasets. 
Below we show an training instance 

{'question': "b'Solve 24 = 1601*c - 1605*c for c.\\n'", 'answer': "b'-6\\n'"}.

We build a custom pipeline using HuggingFace modules. The pipeline includes a seq2seq model, and a tokenizer trained on dataset examples. See [HfMathQA.py](https://gitlab.com/compteligueymc/nlp-tasks/-/blob/main/TrainingPipeline/HfMathQA.py).
